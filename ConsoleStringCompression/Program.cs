﻿using System;
using System.IO;
using System.Text;

namespace ConsoleStringCompression
{
    class Program
    {
    static    string CompressString(string strFileName)
        {
            char activeChar;
            int count = 1;
            StringBuilder CompressedString = new StringBuilder();
            try
            {
                using (StreamReader sr = new StreamReader(strFileName))
                {

                    activeChar = (char)sr.Read();

                    while (!sr.EndOfStream)
                    {
                        if (activeChar == (char)sr.Peek())
                        {
                            count++;
                            sr.Read();
                                
                        }
                        else
                        {
                            if (count > 1)
                            {
                                CompressedString.Append(count.ToString());
                            }
                            CompressedString.Append(activeChar);
                            activeChar = (char)sr.Read();
                            count = 1;
                        }

                        if (sr.EndOfStream)
                        {
                            CompressedString.Append(count.ToString());
                            CompressedString.Append(activeChar);
                        }

                    }


                    
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }

            return CompressedString.ToString();
        }

      static  void compressWithK (string fileName, int k)
        {
            int smallestCompressed= 10000;
            string smallestCompressedValue="";
            StringBuilder originalStr = new StringBuilder();

            try
            {
                using (StreamReader sr = new StreamReader(fileName))
                {
                    originalStr.Append(sr.ReadLine());
                }

                
                    for(int i= 0; i<=originalStr.Length-k; i++)
                    {
                    using (StreamWriter sw = new StreamWriter("./temp.txt"))
                    {
                        originalStr.Remove(i, k);
                        sw.WriteLine(originalStr);
                        originalStr.Clear();
                    }
                    string temp = CompressString("./temp.txt");

                    if (smallestCompressed > temp.Length-3)
                    {

                        smallestCompressedValue = temp;
                        smallestCompressed = temp.Length - 3;
                    }

                    if (File.Exists("./temp.txt"))
                    {
                        File.Delete("./temp.txt");
                    }

                    using (StreamReader sr = new StreamReader(fileName))
                    {
                        originalStr.Append(sr.ReadLine());
                    }
                }

                   
                
            }
            catch (Exception e)
            {
                Console.WriteLine("The process failed: {0}", e.ToString());
            }
            Console.WriteLine("The compressed value is: " + smallestCompressedValue+ "The length is: {0}", smallestCompressed);
        }

        static void Main(string[] args)
        {
            string fileName;
            int k;
            Console.WriteLine("Please enter the absolute or relative path to the file");
            fileName = Console.ReadLine();
            Console.WriteLine("Please enter the K value");
            k =Convert.ToInt32( Console.ReadLine());


            compressWithK(fileName, k);


           
          

            Console.ReadLine();
        }
    }
}
